namespace BOSS.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using BOSS.Data.Models;
    using BOSS.Infrastructure;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public sealed class Configuration : DbMigrationsConfiguration<BOSS.Data.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BOSS.Data.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var user = new ApplicationUser()
            {
                UserName = "admin",
                Email = "admin@boss.com",
                EmailConfirmed = true
            };

            manager.Create(user, "TheBoss");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
            }

            var adminUser = manager.FindByName("admin");

            manager.AddToRoles(adminUser.Id, new string[] { "Admin" });

            var socks = context.Socks.ToList();
            if (socks.Count != 1000)
            {
                context.Database.ExecuteSqlCommand("delete from Socks");
                //var price = Singleton.Instance.GetRandomSockPrice();
                //context.Socks.Add(new Sock()
                //{

                //    Price = price
                //});

                //context.SaveChanges();

                for (int i = 0; i < 1000; i++)
                {
                    var price = Singleton.Instance.GetRandomSockPrice();
                    context.Socks.Add(new Sock()
                    {

                        Price = price
                    });

                    if (i % 100 == 0)
                    {
                        context.SaveChanges();
                    }
                }

                context.SaveChanges();
            }
        }
    }
}
