﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSS.Data.Models
{
    public class Sock
    {
        //[Required]
        [Key]
        public int Id { get; set; }

        [Range(0, double.MaxValue)]
        public decimal Price { get; set; }

        public string ApplicationUserId { get; set; }

        //[ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
