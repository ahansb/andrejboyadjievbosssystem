﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSS.Data.Models
{
    public class Account
    {
        //[Required]
        [Key]
        public int Id { get; set; }

        [Range(0.0, double.MaxValue)]
        public decimal MoneyAmount { get; set; }


        [Required]
        public virtual ApplicationUser ApplicationUser { get; set; } 
    }
}
