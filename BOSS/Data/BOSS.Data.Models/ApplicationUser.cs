﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BOSS.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        private ICollection<Sock> socks;

        public ApplicationUser()
        {
            this.socks = new HashSet<Sock>();
        }


        //public int AccountId { get; set; }

        //[ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public virtual ICollection<Sock> Socks { get { return this.socks; } set { this.socks = value; } }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
