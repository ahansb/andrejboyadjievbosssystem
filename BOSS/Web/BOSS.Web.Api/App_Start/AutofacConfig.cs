﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using BOSS.Data;
using BOSS.Data.Common;
using BOSS.Data.Common.Interfaces;
using BOSS.Data.Interfaces;
using BOSS.Services;
using BOSS.Services.Interfaces;
using BOSS.Web.Api.Controllers;

namespace BOSS.Web.Api
{
    public static class AutofacConfig
    {
        public static void RegisterAutofac()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            builder.RegisterWebApiModelBinderProvider();

            // Register services
            RegisterServices(builder);

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder.Register(x => new ApplicationDbContext())
                .As<DbContext>()
                .InstancePerRequest();


            builder.Register(x=> new ApplicationDbContext())
               .As<IApplicationDbContext>()
               .InstancePerRequest();

            var servicesAssembly = Assembly.GetAssembly(typeof(IUserService));
            builder.RegisterAssemblyTypes(servicesAssembly).AsImplementedInterfaces();

            //builder.RegisterInstance(new XmlDeserializer()).As<IXmlDeserializer>();

            builder.RegisterGeneric(typeof(GenericRepository<>))
                .As(typeof(IGenericRepository<>))
                .InstancePerRequest();

            //builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
            //    .AssignableTo<BaseController>().PropertiesAutowired();
        }
    }
}