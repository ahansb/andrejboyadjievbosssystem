﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOSS.Web.Api.Models.UserViewModels
{
    public class UserResponseViewModel
    {
        public string Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public decimal Money { get; set; }

        public int SocksCount { get; set; }
    }
}