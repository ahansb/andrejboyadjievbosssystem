﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOSS.Web.Api.Models.AccountViewModel
{
    public class AccountResponseViewModel
    {
        public decimal? MoneyAmount { get; set; }
    }
}