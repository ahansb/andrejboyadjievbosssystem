﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BOSS.Infrastructure;
using BOSS.Services.Interfaces;
using Microsoft.AspNet.Identity;

namespace BOSS.Web.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Socks")]
    [Authorize]
    public class SocksController : ApiController
    {
        private readonly ISockService sockService;
        private readonly IUserService userService;

        public SocksController(ISockService sockService, IUserService userService)
        {
            this.sockService = sockService;
            this.userService = userService;
        }

        // GET: api/Socks
        public IHttpActionResult Get()
        {
            var socks = this.sockService.GetAll();
            var use = Singleton.Instance.GetRandomDecimalBetweenZeroAndTwo();

            return this.Ok(new { Use = use, Socks = socks});
        }

        // PUT: api/Socks/5
        public void Put(int id)
        {
            var userId = this.User.Identity.GetUserId();
            var user = this.userService.GetById(userId);

            var sock = this.sockService.GetById(id);

            if (user.Account!=null && user.Account.MoneyAmount>=(sock.Price*Singleton.Instance.Use))
            {
                this.sockService.BuySock(sock, user, sock.Price * Singleton.Instance.Use);
            }
        }
    }
}
