﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using BOSS.Services.Interfaces;
using BOSS.Web.Api.Models.UserViewModels;
using Microsoft.AspNet.Identity;

namespace BOSS.Web.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        // GET: api/Users
        [Authorize(Roles = "Admin")]
        public IHttpActionResult Get()
        {
            var users = this.userService.GetAll().Select(u=>new UserResponseViewModel() { Id = u.Id, Username = u.UserName, Email= u.Email, Money = u.Account!=null ? u.Account.MoneyAmount:0 }).ToList();
            return this.Ok(users);
        }

        [Route("profile")]
        [HttpGet]
        public IHttpActionResult GetMyProfile()
        {
           var userId = this.User.Identity.GetUserId();
            var user = this.userService.GetById(userId);
            
            var response = new UserResponseViewModel()
            { 
                Id = user.Id,
                Username = user.UserName,
                Email = user.Email,
                Money = user.Account != null ? user.Account.MoneyAmount : 0,
                SocksCount = user.Socks.Count
            };
            return this.Ok(response);
        }
    }
}
