﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using BOSS.Data.Models;
using BOSS.Services.Interfaces;
using Microsoft.AspNet.Identity;

namespace BOSS.Web.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class MoneyAccountController : ApiController
    {
        private readonly IUserService userService;

        private readonly IAccountService accountService;

        public MoneyAccountController(IAccountService accountService,IUserService userService)
        {
            this.accountService = accountService;
            this.userService = userService;
        }

        // POST: api/MoneyAccount
        [HttpPost]
        public IHttpActionResult Post([FromBody]decimal amount)
        {
            if (amount <= 0)
            {
                return this.BadRequest();
            }

            var userId = this.User.Identity.GetUserId();
            var user = this.userService.GetById(userId);
            if (user.Account == null)
            {
                accountService.Create(new Account()
                {
                    ApplicationUser = user,
                    MoneyAmount = amount
                });
            }
            else
            {
                accountService.Deposit(user.Account.Id, amount);
            }

            return this.Ok();
        }
    }
}
