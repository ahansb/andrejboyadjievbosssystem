﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSS.Data.Common.Interfaces;
using BOSS.Data.Models;
using BOSS.Services.Interfaces;

namespace BOSS.Services
{
    public class AccountService : IAccountService
    {
        private readonly IGenericRepository<Account> accountRepository;

        public AccountService(IGenericRepository<Account> accountRepository)
        {
            this.accountRepository = accountRepository;
        }

        public int Create(Account account)
        {
            this.accountRepository.Add(account);
            this.accountRepository.SaveChanges();
            var id = account.Id;
            return id;
        }

        public void Deposit(int id, decimal amount)
        {
            var account = this.accountRepository.GetById(id);
            account.MoneyAmount += amount;
            this.accountRepository.Update(account);
            this.accountRepository.SaveChanges();
        }
    }
}
