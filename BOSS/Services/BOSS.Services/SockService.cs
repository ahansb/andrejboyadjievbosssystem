﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSS.Data.Common.Interfaces;
using BOSS.Data.Models;
using BOSS.Services.Interfaces;

namespace BOSS.Services
{
    public class SockService : ISockService
    {
        private readonly IGenericRepository<Sock> sockRepository;
        private readonly IGenericRepository<Account> accountRepository;

        public SockService(IGenericRepository<Sock> sockRepository, IGenericRepository<Account> accountRepository)
        {
            this.sockRepository = sockRepository;
            this.accountRepository = accountRepository;
        }

        public IQueryable<Sock> GetAll()
        {
            var result = this.sockRepository.All().Where(s=>s.ApplicationUserId == null);
            return result;
        }

        public Sock GetById(int id)
        {
            var result = this.sockRepository.GetById(id);
            return result;
        }

        public void BuySock(Sock sock, ApplicationUser user, decimal ammount)
        {
            sock.ApplicationUser = user;
            sock.Price = ammount;
            this.sockRepository.Update(sock);

            user.Account.MoneyAmount -= ammount;
            this.accountRepository.Update(user.Account);
            this.sockRepository.SaveChanges();
        }
    }
}
