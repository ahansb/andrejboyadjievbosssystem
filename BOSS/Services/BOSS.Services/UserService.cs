﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSS.Data.Common.Interfaces;
using BOSS.Data.Models;
using BOSS.Services.Interfaces;

namespace BOSS.Services
{
    public class UserService:IUserService
    {
        private readonly IGenericRepository<ApplicationUser> applicationUserRepository;

        public UserService(IGenericRepository<ApplicationUser> applicationUserRepository)
        {
            this.applicationUserRepository = applicationUserRepository;
        }

        public IQueryable<ApplicationUser> GetAll()
        {
            var result = this.applicationUserRepository.All();
            return result;
        }

        public ApplicationUser GetById(string id)
        {
            var result = this.applicationUserRepository.GetById(id);
            return result;
        }
    }
}
