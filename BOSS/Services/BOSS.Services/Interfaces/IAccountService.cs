﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSS.Data.Models;

namespace BOSS.Services.Interfaces
{
    public interface IAccountService
    {
        void Deposit(int id, decimal amount);

        int Create(Account account);
    }
}
