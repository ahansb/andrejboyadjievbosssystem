﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSS.Data.Models;

namespace BOSS.Services.Interfaces
{
    public interface ISockService
    {
        IQueryable<Sock> GetAll();

        Sock GetById(int id);

        void BuySock(Sock sock, ApplicationUser user, decimal ammount);
    }
}
