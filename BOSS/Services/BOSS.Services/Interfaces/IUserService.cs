﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSS.Data.Models;

namespace BOSS.Services.Interfaces
{
    public interface IUserService
    {
        IQueryable<ApplicationUser> GetAll();

        ApplicationUser GetById(string id);
    }
}
