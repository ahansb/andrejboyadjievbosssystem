﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSS.Infrastructure
{
    public sealed class Singleton
    {
        private static readonly Lazy<Singleton> lazy = new Lazy<Singleton>(() => new Singleton());
        private readonly Random rnd;
        private DateTime dateTime;
        private decimal use;

        public static Singleton Instance { get { return lazy.Value; } }

        private Singleton()
        {
            rnd = new Random();
        }

        public decimal Use { get { return this.use; } }

        public decimal GetRandomDecimalBetweenZeroAndTwo()
        {
            if (this.dateTime.AddHours(1) > DateTime.Now)
            {
                return this.use;
            }

            var number = "";
            var randomDigit = rnd.Next(3);

            for (int i = 0; i < 2; i++)
            {
                if (randomDigit == 2)
                {
                    randomDigit = rnd.Next(3);
                }
            }

            if (randomDigit == 2)
            {
                number = "2.0";
            }
            else
            {
                number += randomDigit;
                number += ".";
                for (int i = 0; i < 30; i++)
                {
                    number += rnd.Next(10);
                }
            }

            this.use = decimal.Parse(number);
            this.dateTime = DateTime.Now;

            return use;
        }

        public decimal GetRandomSockPrice()
        {
            var first = rnd.Next(1, 100);
            var second = rnd.Next(1, 100);
            var third = rnd.Next(1,100);
            decimal result = (decimal)(first * second) / (decimal)third; 

            return result;
        }
    }
}
